
#include <memory>
#include <limits>
#include <cmath>
#include "KdTree.h"

KdNode::KdNode(punt2 punt) : punt(punt) {}

UKdNode &KdNode::giveChild(bool left)
{
    if (left) {
        return this->left;
    }

    return this->right;
}

KdTree::KdTree(punt2&& punt) :
    root(std::make_unique<KdNode>(std::move(punt))) {}

UKdNode& KdTree::search(const punt2& p) {
    UKdNode* node = &root;
    bool x = true;

    while (*node) {
        if ((*node)->punt == p) {
            return *node;
        }

        if (x) {
            node = &(*node)->giveChild(p.x < (*node)->punt.x);
        } else {
            node = &(*node)->giveChild(p.y < (*node)->punt.y);
        }

        x = !x;
    }

    return *node;
}

void KdTree::add(punt2&& punt) {
    UKdNode& node = search(punt);
    if (!node) {
        node = std::make_unique<KdNode>(std::move(punt));
    } else {
        throw std::runtime_error("Point already exists!");
    }
}

punt2 KdTree::closest(const punt2 &p, int& visitedAmount) {
    if (root) {
        punt2 best(root->punt);
        visitedAmount = 1;
        root->closestRec(p, best, visitedAmount, 0);
        return best;
    }

    return {};
}
void KdNode::closestRec(const punt2 &p, punt2 &best, int &visitedAmount, int level) {
    bool left;

    if (level == 0) {
        left = p.x < punt.x;
    } else {
        left = p.y < punt.y;
    }

    UKdNode& node = giveChild(left);
    if (node) {
        node->closestRec(p, best, ++visitedAmount, (level+1) % 2);
    }

    UKdNode& other = giveChild(!left);
    if (other) {
        if (level == 0) {
            if (abs(punt.x - p.x) < p.kwadafstand(best)) {
                other->closestRec(p, best, ++visitedAmount, (level+1) % 2);
            }
        } else {
            if (abs(punt.y - p.y) < p.kwadafstand(best)) {
                other->closestRec(p, best, ++visitedAmount, (level+1) % 2);
            }
        }
    }

    if (p.kwadafstand(punt) < p.kwadafstand(best)) {
        best = punt;
    }
}
