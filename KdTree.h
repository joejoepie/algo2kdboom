
#ifndef KD_TREE_H
#define KD_TREE_H

#include "punt2.h"

class KdNode;
class KdTree;
typedef std::unique_ptr<KdNode> UKdNode;

class KdNode {
public:
    KdNode(punt2 punt);
    UKdNode& giveChild(bool left);

    punt2 punt;
    UKdNode left, right;
private:
    void closestRec(const punt2 &p, punt2 &best, int &visitedAmount, int level);
    friend KdTree;
};

class KdTree {
public:
    KdTree(punt2&& punt);
    KdTree() = default;

    UKdNode& search(const punt2& punt);
    punt2 closest(const punt2& punt, int& visitedAmount);
    void add(punt2&& punt);
private:
    UKdNode root;
};


#endif
